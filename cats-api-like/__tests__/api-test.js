const supertest = require('supertest');
const app = require('../src/routes');
const {pool}=require('../src/storage');
const {logger} = require('../src/logger');

logger.transports[0].silent = true;
const catIds = [1, 5];

beforeAll(async (done) => {
  await pool.query('TRUNCATE cats CASCADE');
  await Promise.all([
    pool.query(
        'INSERT INTO Cats(id, name, description, gender) VALUES ($1, $2, $3, $4)',
        [catIds[0], 'testCats_1', 'testCats', 'male']),
    pool.query(
        'INSERT INTO Cats(id, name, description, gender) VALUES ($1, $2, $3, $4)',
        [catIds[1], 'testCats_2', 'testCats', 'male'])]);

  done();
});
afterAll((done) => {
  pool.end();
  done();
});

describe('Test like', () => {
  it('Delete like from cat without like', async (done) => {
    const response = await supertest(app)
        .delete(`/cats/${catIds[0]}/like`);
    expect(response.status).toEqual(200);
    expect(response.body.likes).toEqual(0);
    done();
  });

  it('Set like for invalid catId', async (done) => {
    const response = await supertest(app)
        .post('/cats/-1/like');
    expect(response.status).toEqual(404);
    expect(response.body.output.payload.error).toEqual('Not Found');
    expect(response.body.output.payload.message).toEqual('Кот с id=-1 не найден');
    done();
  });

  it('Set like catId', async (done) => {
    const response = await supertest(app)
        .post(`/cats/${catIds[1]}/like`);
    expect(response.status).toEqual(200);
    expect(response.body).toEqual({
      'id': 5,
      'name': 'testCats_2',
      'description': 'testCats',
      'tags': null,
      'gender': 'male',
      'likes': 1,
      'dislikes': 0,
    });
    done();
  });

  it('Delete like catId', async (done) => {
    const response = await supertest(app)
        .delete(`/cats/${catIds[1]}/like`);
    expect(response.status).toEqual(200);
    expect(response.body).toEqual({
      'id': 5,
      'name': 'testCats_2',
      'description': 'testCats',
      'tags': null,
      'gender': 'male',
      'likes': 0,
      'dislikes': 0,
    });
    done();
  });

  it('Delete like for invalid catId', async (done) => {
    const response = await supertest(app)
        .delete(`/cats/-1/like`);
    expect(response.status).toEqual(404);
    expect(response.body.output.payload.error).toEqual('Not Found');
    expect(response.body.output.payload.message).toEqual('Кот с id=-1 не найден');
    done();
  });
});

describe('Test dislike', () => {
  it('Delete dislike from cat without dislike', async (done) => {
    const response = await supertest(app)
        .delete(`/cats/${catIds[0]}/dislike`);
    expect(response.status).toEqual(200);
    expect(response.body.dislikes).toEqual(0);
    done();
  });

  it('Set dislike for invalid catId', async (done) => {
    const response = await supertest(app)
        .post('/cats/-1/dislike');
    expect(response.status).toEqual(404);
    expect(response.body.output.payload.error).toEqual('Not Found');
    expect(response.body.output.payload.message).toEqual('Кот с id=-1 не найден');
    done();
  });

  it('Set dislike catId', async (done) => {
    const response = await supertest(app)
        .post(`/cats/${catIds[1]}/dislike`);
    expect(response.status).toEqual(200);
    expect(response.body).toEqual({
      'id': 5,
      'name': 'testCats_2',
      'description': 'testCats',
      'tags': null,
      'gender': 'male',
      'likes': 0,
      'dislikes': 1,
    });
    done();
  });

  it('Delete dislike catId', async (done) => {
    const response = await supertest(app)
        .delete(`/cats/${catIds[1]}/dislike`);
    expect(response.status).toEqual(200);
    expect(response.body).toEqual({
      'id': 5,
      'name': 'testCats_2',
      'description': 'testCats',
      'tags': null,
      'gender': 'male',
      'likes': 0,
      'dislikes': 0,
    });
    done();
  });

  it('Delete dislike for invalid catId', async (done) => {
    const response = await supertest(app)
        .delete(`/cats/-1/dislike`);
    expect(response.status).toEqual(404);
    expect(response.body.output.payload.error).toEqual('Not Found');
    expect(response.body.output.payload.message).toEqual('Кот с id=-1 не найден');
    done();
  });
  describe('Test rating', () => {
    it('Get rating', async (done)=>{
      const response = await supertest(app).get('/cats/rating');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual({
        likes: expect.arrayContaining([
          {id: expect.any(Number),
            name: expect.any(String),
            likes: expect.any(Number),
          },
        ]),
        dislikes: expect.arrayContaining([
          {id: expect.any(Number),
            name: expect.any(String),
            dislikes: expect.any(Number),
          },
        ]),
      });
      done();
    });

    it('Get dislikes-rating', async (done)=>{
      const response = await supertest(app).get('/cats/dislikes-rating');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual(expect.arrayContaining([{
        id: expect.any(Number),
        name: expect.any(String),
        dislikes: expect.any(Number),
      },
      ]));
      done();
    });

    it('Get likes-rating', async (done)=>{
      const response = await supertest(app).get('/cats/likes-rating');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual(expect.arrayContaining([{
        id: expect.any(Number),
        name: expect.any(String),
        likes: expect.any(Number),
      },
      ]));
      done();
    });
  });
  describe('Test likes/dislikes one method', () => {
    it('Set like and dislike in one request ', async (done)=>{
      const response = await supertest(app)
          .post(`/cats/${catIds[1]}/likes`)
          .send({
            like: true,
            dislike: true,
          });
      expect(response.status).toEqual(400);
      expect(response.body.output.payload.error).toEqual('Bad Request');
      expect(response.body.output.payload.message).toEqual('Нельзя установить одновременно лайк и дизлайк');
      done();
    });

    it('Set like', async (done)=>{
      const response = await supertest(app)
          .post(`/cats/${catIds[1]}/likes`)
          .send({
            like: true,
          });
      expect(response.status).toEqual(200);
      expect(response.body).toEqual({
        'id': 5,
        'name': 'testCats_2',
        'description': 'testCats',
        'tags': null,
        'gender': 'male',
        'likes': 1,
        'dislikes': 0,
      });
      done();
    });

    it('Set dislike', async (done)=>{
      const response = await supertest(app)
          .post(`/cats/${catIds[1]}/likes`)
          .send({
            dislike: true,
          });
      expect(response.status).toEqual(200);
      expect(response.body).toEqual({
        'id': 5,
        'name': 'testCats_2',
        'description': 'testCats',
        'tags': null,
        'gender': 'male',
        'likes': 1,
        'dislikes': 1,
      });
      done();
    });
    it('Set dislike unset like', async (done)=>{
      const response = await supertest(app)
          .post(`/cats/${catIds[1]}/likes`)
          .send({
            like: false,
            dislike: true,
          });
      expect(response.status).toEqual(200);
      expect(response.body).toEqual({
        'id': 5,
        'name': 'testCats_2',
        'description': 'testCats',
        'tags': null,
        'gender': 'male',
        'likes': 0,
        'dislikes': 2,
      });
      done();
    });
    it('Set like unset dislike', async (done)=>{
      const response = await supertest(app)
          .post(`/cats/${catIds[1]}/likes`)
          .send({
            like: true,
            dislike: false,
          });
      expect(response.status).toEqual(200);
      expect(response.body).toEqual({
        'id': 5,
        'name': 'testCats_2',
        'description': 'testCats',
        'tags': null,
        'gender': 'male',
        'likes': 1,
        'dislikes': 1,
      });
      done();
    });
    it('Unset like unset dislike', async (done)=>{
      const response = await supertest(app)
          .post(`/cats/${catIds[1]}/likes`)
          .send({
            like: false,
            dislike: false,
          });
      expect(response.status).toEqual(200);
      expect(response.body).toEqual({
        'id': 5,
        'name': 'testCats_2',
        'description': 'testCats',
        'tags': null,
        'gender': 'male',
        'likes': 0,
        'dislikes': 0,
      });
      done();
    });
  });
});
